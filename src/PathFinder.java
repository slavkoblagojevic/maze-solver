import java.awt.Point;
import java.util.*;

public class PathFinder {

    public static List<Point> getShortestPath(int[][] matrix, Point start, Point end) {
        Queue<Point> queue = new LinkedList<>();
        Map<Point, Point> parent = new HashMap<>();

        queue.add(start);
        parent.put(start, null);

        while (!queue.isEmpty()) {
            Point current = queue.remove();
            if (current.equals(end)) {
                break;
            }

            int x = current.x;
            int y = current.y;

            if (x > 0 && matrix[y][x - 1] == 0 && !parent.containsKey(new Point(x - 1, y))) { // Left
                Point next = new Point(x - 1, y);
                queue.add(next);
                parent.put(next, current);
            }

            if (y > 0 && matrix[y - 1][x] == 0 && !parent.containsKey(new Point(x, y - 1))) { // Up
                Point next = new Point(x, y - 1);
                queue.add(next);
                parent.put(next, current);
            }

            if (x < matrix[0].length - 1 && matrix[y][x + 1] == 0 && !parent.containsKey(new Point(x + 1, y))) { // Right
                Point next = new Point(x + 1, y);
                queue.add(next);
                parent.put(next, current);
            }

            if (y < matrix.length - 1 && matrix[y + 1][x] == 0 && !parent.containsKey(new Point(x, y + 1))) { // Down
                Point next = new Point(x, y + 1);
                queue.add(next);
                parent.put(next, current);
            }
        }

        List<Point> path = new ArrayList<>();
        Point current = end;
        while (current != null) {
            path.add(current);
            current = parent.get(current);
        }
        Collections.reverse(path);

        return path;
    }
}
