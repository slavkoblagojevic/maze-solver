
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class MazeSolver {
	public static void main(String[] args) throws IOException {

		String maze = "";
		Scanner input = new Scanner(System.in);
		System.out.println("Please choose an option: ");
		System.out.println("1- Maze1.png");
		System.out.println("2- Maze2.png");
		System.out.println("3- Maze3.png");

        while (!input.hasNextInt()) {
            System.out.println("Invalid input. Please enter a number.");
            input.next();
        }
		
		int userInput = input.nextInt();
		input.close();

		switch (userInput) {
		case 1:
			maze = "maze1.png";
			break;
		case 2:
			maze = "maze2.png";
			break;
		case 3:
			maze = "maze3.png";
			break;
		default:
			System.out.println("Invalid input.");
			return;
		}

		BufferedImage image = ImageIO.read(new File(maze));

		int width = image.getWidth();
		int height = image.getHeight();
		int[][] matrix = new int[height][width];
		Point start = null;
		Point end = null;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int color = image.getRGB(x, y);

				// start point
				if (image.getRGB(x, height - 1) == Color.WHITE.getRGB()) {
					start = new Point(x, height - 1);
				}
				// end point
				if (image.getRGB(x, 0) == Color.WHITE.getRGB()) {
					end = new Point(x, 0);
				}

				if (color == Color.BLACK.getRGB()) {
					matrix[y][x] = 1; // wall

				} else {
					matrix[y][x] = 0; // path

				}
			}
		}

		if (start == null || end == null) {
			System.out.println("Start or end Point does not exist!");
			return;
		}

		List<Point> path = PathFinder.getShortestPath(matrix, start, end);

		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int color = image.getRGB(x, y);
				result.setRGB(x, y, color);
			}
		}

		for (Point p : path) {
			result.setRGB(p.x, p.y, Color.GREEN.getRGB());
		}

		String randomString = UUID.randomUUID().toString().substring(0, 8);
		ImageIO.write(result, "png", new File("maze_" + randomString + ".png"));
	}
}